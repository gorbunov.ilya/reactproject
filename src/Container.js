import React from 'react';
function Container() {
    return (
        <div className="board" id="board">
            <div className="container" id="container">
                <div className="row">
                    <div id="film-1" className="col-12 py-3 col-sm-6 col-md-4 col-lg-4"></div>
                    <div id="film-2" className="col-12 py-3 col-sm-6 col-md-4 col-lg-4"></div>
                    <div id="film-3" className="col-12 py-3 col-sm-6 col-md-4 col-lg-4"></div>
                </div>
            </div>
        </div>
    );
}
export default Container;