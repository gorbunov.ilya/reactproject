import React from 'react';
function Sidebar() {
    return (
        <nav className="navbar navbar-expand-lg main-nav">
            <button className="navbar-toggler navbar-btn" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <i className="fas fa-bars"></i>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav mt-2 ml-4">
                    <li className="nav-item active mr-4">
                        <a className="nav-link" href="111.ru">New</a>
                    </li>
                    <li className="nav-item mr-4">
                        <a className="nav-link" href="111.ru">Detective</a>
                    </li>
                    <li className="nav-item mr-4">
                        <a className="nav-link" href="111.ru">Fantastic</a>
                    </li>
                    <li className="nav-item mr-4">
                        <a className="nav-link" href="111.ru">Team</a>
                    </li>
                </ul>
            </div>
        </nav>

    );
}
export default Sidebar;