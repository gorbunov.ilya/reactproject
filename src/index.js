import React from 'react';
import ReactDOM from 'react-dom';
import './bootstrap.min.css';
import './index.css';
import './style.css';
import Container from './Container';
import Film from './Film';
import * as serviceWorker from './serviceWorker';
import Header from './Header';
import Footer from './Footer';
import Sidebar from './Sidebar';

ReactDOM.render(<Header />, document.getElementById('root'));

ReactDOM.render(<Container />, document.getElementById('root')
.appendChild(document.createElement('div')));

ReactDOM.render(<Footer />, document.getElementById('root')
.appendChild(document.createElement('div')));

ReactDOM.render(<Sidebar />, document.getElementById('header'));

ReactDOM.render(<Film name = "Доктор Стрэндж" 
img = "https://cdn.fishki.net/upload/post/2016/11/16/2137791/36a8a81f59767c3e14040d19a4b87113.jpg"
date = "Год выхода: 2016"
site = "https://www.kinopoisk.ru/film/409600/ord/rnd/rnd/1546186638/"/>,
document.getElementById('film-1'));

ReactDOM.render(<Film name = "Варкрафт" 
img = "https://cdn.fishki.net/upload/post/2016/11/16/2137791/tn/64cff7632e04c4418f6fbf658cd45062.jpg" 
date = "Год выхода: 2016"
site = "https://www.kinopoisk.ru/film/277328/"/>,
document.getElementById('film-2'));

ReactDOM.render(<Film name = "Капитан Марвел" 
img = "https://st.kp.yandex.net/images/film_iphone/iphone360_843859.jpg" 
date = "Год выхода: 2019"
site = "https://www.kinopoisk.ru/film/843859/"/>,
document.getElementById('film-3'));

serviceWorker.unregister();
