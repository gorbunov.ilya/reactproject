import React from 'react';
function Film(props) {
    return (
            <div className="card">
                <a href={props.site}>
                <img className="card-img-top" src={props.img} alt=""></img>
                </a>
                <div className="card-body">
                <h4>
                    {props.name}
                </h4>
                <p>
                    {props.date}
                </p>
                </div>
            </div>
    );
}
export default Film;